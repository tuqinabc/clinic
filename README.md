# what has done in this project:
In this simple project, jpa, spring event have been utilized.
1. A patient can search for different providers of the clinic by provider key id(first name and last name) or find all providers
    - GET localhost:8080/providers
    - GET localhost:8080/provider/{firstname}/{lastname}
    
2. A patient can look for the availabilities of specific provider within a defined time interval
    - POST localhost:8080/provider/availabilities
    
3. A patient can book an appointment with a provider by selecting one of their availabilities
    - POST localhost:8080/appointment
    - add record to appointment and remove the corresponding time slot in this provider availabilities
    
4. to simplify test, h2 in-memory be used and some dummy data has been added at the beginning of project starting by using spring event. 

5. a couple of tests have been implemented, including all features mentioned above.

    
   
package org.qtcoding.demo.clinic.event;

import org.qtcoding.demo.clinic.service.ClinicManagerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.context.ServletWebServerInitializedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;


// The only purpose of this class is to provide the data for test.
@Component
public class ApplicationStart {
    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationStart.class);
    @Autowired
    ClinicManagerService clinicManagerService;
    @EventListener
    public void handleEvent(ServletWebServerInitializedEvent event) {
        LOGGER.info("[Pomelo]Preparing the data for test...");
        clinicManagerService.prepareData();
    }
}

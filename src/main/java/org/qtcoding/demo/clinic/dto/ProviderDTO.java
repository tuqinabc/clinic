package org.qtcoding.demo.clinic.dto;

import org.qtcoding.demo.clinic.entity.Availability;

import java.io.Serializable;

public class ProviderDTO implements Serializable {
    private String firstName;
    private String lastName;
    private Availability availability;

    public ProviderDTO() {
    }

    public ProviderDTO(String firstName, String lastName, Availability availability) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.availability = availability;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Availability getAvailability() {
        return availability;
    }

    public void setAvailability(Availability availability) {
        this.availability = availability;
    }

}

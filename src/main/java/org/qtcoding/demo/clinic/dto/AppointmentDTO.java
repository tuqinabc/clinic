package org.qtcoding.demo.clinic.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.qtcoding.demo.clinic.entity.Availability;
import org.qtcoding.demo.clinic.entity.IdKey;
import org.qtcoding.demo.clinic.entity.Patient;
import org.qtcoding.demo.clinic.entity.Provider;

import java.io.Serializable;

public class AppointmentDTO implements Serializable {
    private Patient patient;
    private Provider provider;
    private Availability availability;

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    public Availability getAvailability() {
        return availability;
    }

    public void setAvailability(Availability availability) {
        this.availability = availability;
    }

    @JsonIgnore
    public IdKey getProviderId() {
        return new IdKey(provider.getFirstName(), provider.getLastName());
    }

    @JsonIgnore
    public IdKey getPatientId() {
        return new IdKey(patient.getFirstName(), patient.getLastName());
    }
}

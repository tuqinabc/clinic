package org.qtcoding.demo.clinic.service;

import org.apache.juli.logging.LogFactory;
import org.qtcoding.demo.clinic.entity.Availability;
import org.qtcoding.demo.clinic.entity.Clinic;
import org.qtcoding.demo.clinic.entity.Patient;
import org.qtcoding.demo.clinic.entity.Provider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class ClinicManagerService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ClinicManagerService.class);

    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void prepareData() {
        LOGGER.debug("Start of prepare data");
        final Clinic clinic = new Clinic();
        clinic.setName("Pomelo's clinic");

        final Patient patient01 = new Patient("Peter", "Q");
        final Patient patient02 = new Patient("Mario", "S");


        final Provider provider01 = new Provider("Emma", "L");
        final Provider provider02 = new Provider("Mina", "T");

        List<Availability> availabilities01 = allDayAvailable(new Availability(LocalDateTime.of(2021, 1, 10, 9, 0), LocalDateTime.of(2021, 1, 10, 17, 0)));
        List<Availability> availabilities02 = allDayAvailable(new Availability(LocalDateTime.of(2021, 1, 11, 9, 0), LocalDateTime.of(2021, 1, 11, 17, 0)));
        provider01.getAvailabilities().addAll(availabilities01);
        provider02.getAvailabilities().addAll(availabilities02);

        clinic.getPatients().add(patient01);
        clinic.getPatients().add(patient02);
        clinic.getProviders().add(provider01);
        clinic.getProviders().add(provider02);

        em.persist(clinic);
    }


    // helper method to generate list of availabilities
    private List<Availability> allDayAvailable(Availability availability) {
        List<Availability> availabilities = new ArrayList<>();
        LocalDateTime startDateTime = availability.getStartDateTime();
        final LocalDateTime endDateTime = availability.getEndDateTime();
        // Roughly to generate time slots
        while (startDateTime.isBefore(endDateTime)) {
            availabilities.add(new Availability(startDateTime));
            startDateTime = startDateTime.plusMinutes(15);
        }
        return availabilities;
    }

}

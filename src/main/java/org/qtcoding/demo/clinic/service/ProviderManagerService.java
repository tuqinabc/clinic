package org.qtcoding.demo.clinic.service;

import org.qtcoding.demo.clinic.entity.*;
import org.qtcoding.demo.clinic.repository.AppointmentRepository;
import org.qtcoding.demo.clinic.repository.ProviderRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class ProviderManagerService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProviderManagerService.class);
    @Autowired
    ProviderRepository providerRepository;
    @Autowired
    AppointmentRepository appointmentRepository;

    public Provider findProviderById(IdKey providerId) {
        return providerRepository.findById(providerId).orElse(null);
    }

    public List<Provider> findAllProviders() {
        return providerRepository.findAll();
    }

    public List<Availability> findAvailabilityListOfProvider(IdKey providerId, Availability availability) {
        LOGGER.debug("Start of finding availability list of provider.");
        final Provider provider = findProviderById(providerId);
        final List<Availability> availabilities = new ArrayList<>();
        if (provider != null) {
            final List<Availability> availabilityList = provider.getAvailabilities();
            if (availability == null || availability.getStartDateTime() == null || availability.getEndDateTime() == null) {
                return availabilityList;
            }
            availabilityList.stream().filter(it ->
                    (it.getStartDateTime().isAfter(availability.getStartDateTime()) ||
                            it.getStartDateTime().isEqual(availability.getStartDateTime())) &&
                            (it.getEndDateTime().isBefore(availability.getEndDateTime()) ||
                                    it.getEndDateTime().isEqual(availability.getEndDateTime()))
            ).forEach(availabilities::add);
        }
        return availabilities;
    }

    @Transactional
    public boolean bookAppointment(Patient patient, IdKey providerId, Availability availability) {
        LOGGER.debug("Start of booking appointment.");
        final Appointment appointment = new Appointment();
        final Provider provider = findProviderById(providerId);
        appointment.setProvider(provider);
        appointment.setPatient(patient);
        appointment.setStartDateTime(availability.getStartDateTime());
        appointment.setEndDateTime(availability.getEndDateTime());
        appointmentRepository.save(appointment);
        final Availability returnedAvailability = provider.getAvailabilities().stream().filter(it -> it.getStartDateTime().isEqual(availability.getStartDateTime()) &&
                it.getEndDateTime().isEqual(availability.getEndDateTime())).findFirst().orElse(null);
        if (returnedAvailability != null) {
            provider.getAvailabilities().remove(returnedAvailability);
            providerRepository.save(provider);
            LOGGER.debug("Availability of given provider has been updated.");
            return true;
        } else {
            LOGGER.debug("Can not book an appointment due to invalid availability.");
            return false;
        }
    }
}

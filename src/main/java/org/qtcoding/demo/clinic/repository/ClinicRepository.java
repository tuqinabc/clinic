package org.qtcoding.demo.clinic.repository;

import org.qtcoding.demo.clinic.entity.Clinic;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClinicRepository extends JpaRepository<Clinic, String> {
}

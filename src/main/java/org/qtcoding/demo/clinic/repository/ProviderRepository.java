package org.qtcoding.demo.clinic.repository;

import org.qtcoding.demo.clinic.entity.IdKey;
import org.qtcoding.demo.clinic.entity.Provider;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProviderRepository extends JpaRepository<Provider, IdKey> {
}

package org.qtcoding.demo.clinic.repository;

import org.qtcoding.demo.clinic.entity.IdKey;
import org.qtcoding.demo.clinic.entity.Patient;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PatientRepository extends JpaRepository<Patient, IdKey> {
}

package org.qtcoding.demo.clinic.controller;

import org.qtcoding.demo.clinic.dto.AppointmentDTO;
import org.qtcoding.demo.clinic.dto.ProviderDTO;
import org.qtcoding.demo.clinic.entity.Availability;
import org.qtcoding.demo.clinic.entity.IdKey;
import org.qtcoding.demo.clinic.entity.Provider;
import org.qtcoding.demo.clinic.service.ProviderManagerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ClinicManagementController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ClinicManagementController.class);
    @Autowired
    ProviderManagerService providerManagerService;

    // A patient can search for different providers of the clinic
    @GetMapping("/providers")
    List<Provider> findProviders() {
        LOGGER.debug("Start of findProvider()");
        return providerManagerService.findAllProviders();
    }

    // A patient can find all providers of the clinic
    @GetMapping("/provider/{firstname}/{lastname}")
    Provider findProvidersById(@PathVariable final String firstname, @PathVariable final String lastname) {
        LOGGER.debug("Start of findProviderById() with firstName: {}, lastName: {}", firstname, lastname);
        final IdKey providerId = new IdKey(firstname, lastname);
        final Provider providers = providerManagerService.findProviderById(providerId);
        return providers;
    }

    // A patient can look for the availabilities of specific provider within a defined time interval
    @PostMapping("/provider/availabilities")
    List<Availability> findProviderByAvailabilities(@RequestBody final ProviderDTO providerDto) {
        LOGGER.debug("Start of findProviderByAvailabilities(), providerDto: {}", providerDto);
        final IdKey providerId = new IdKey(providerDto.getFirstName(), providerDto.getLastName());
        return providerManagerService.findAvailabilityListOfProvider(providerId, providerDto.getAvailability());
    }

    // A patient can book an appointment with a provider by selecting one of their availabilities
    @PostMapping("/appointment")
    boolean bookAppointment(@RequestBody final AppointmentDTO appointmentDto) {
        LOGGER.debug("Start of bookAppointment(), appointmentDto: {}", appointmentDto);
        return providerManagerService.bookAppointment(appointmentDto.getPatient(), appointmentDto.getProviderId(), appointmentDto.getAvailability());
    }
}

package org.qtcoding.demo.clinic.entity;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@IdClass(IdKey.class)
public class Provider implements Serializable {
    @Id
    private String firstName;
    @Id
    private String lastName;

    @ElementCollection
    private List<Availability> availabilities = new ArrayList<>();

    public Provider() {
    }

    public Provider(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Provider(String firstName, String lastName, List<Availability> availabilities) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.availabilities = availabilities;
    }

    public List<Availability> getAvailabilities() {
        return availabilities;
    }

    public void setAvailabilities(List<Availability> availabilities) {
        this.availabilities = availabilities;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

}

package org.qtcoding.demo.clinic.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
public class Appointment implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long appointmentId;
    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "patient_firstname"),
            @JoinColumn(name = "patient_lastname")
    })
    private Patient patient;
    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "provider_firstname"),
            @JoinColumn(name = "provider_lastname")
    })
    private Provider provider;
    private LocalDateTime startDateTime;
    private LocalDateTime endDateTime;

    public long getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(long appointmentId) {
        this.appointmentId = appointmentId;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    public LocalDateTime getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(LocalDateTime startDateTime) {
        this.startDateTime = startDateTime;
    }

    public LocalDateTime getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(LocalDateTime endDateTime) {
        this.endDateTime = endDateTime;
    }
}

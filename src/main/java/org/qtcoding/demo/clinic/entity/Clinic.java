package org.qtcoding.demo.clinic.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Clinic implements Serializable {
    @Id
    private String name;
    @OneToMany(cascade = CascadeType.ALL)
    private List<Patient> patients = new ArrayList<>();
    @OneToMany(cascade = CascadeType.ALL)
    private List<Provider> providers = new ArrayList<>();

    public Clinic(String name) {
        this.name = name;
    }

    public Clinic() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Patient> getPatients() {
        return patients;
    }

    public void setPatients(List<Patient> patients) {
        this.patients = patients;
    }

    public List<Provider> getProviders() {
        return providers;
    }

    public void setProviders(List<Provider> providers) {
        this.providers = providers;
    }
}

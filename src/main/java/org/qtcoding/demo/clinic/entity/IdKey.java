package org.qtcoding.demo.clinic.entity;

import java.io.Serializable;

public class IdKey implements Serializable {
    private String firstName;
    private String lastName;

    public IdKey() {
    }

    public IdKey(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }
}

package org.qtcoding.demo.clinic.controller;

import org.qtcoding.demo.clinic.dto.AppointmentDTO;
import org.qtcoding.demo.clinic.dto.ProviderDTO;
import org.qtcoding.demo.clinic.entity.Availability;
import org.qtcoding.demo.clinic.entity.Patient;
import org.qtcoding.demo.clinic.entity.Provider;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.LocalDateTime;
import java.util.List;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class ClinicManagementControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;
    final private String URL = "http://localhost:8080";

    @Test
    void testSearchProviders() {
        ResponseEntity<List> responseEntity = restTemplate.getForEntity(URL + "/providers", List.class);
        Assertions.assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
        Assertions.assertEquals(responseEntity.getBody().size(), 2);
    }

    @Test
    void testSearchProviderById() {
        ResponseEntity<Provider> responseEntity = restTemplate.getForEntity(URL + "/provider/Emma/L", Provider.class);
        Assertions.assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
        Assertions.assertEquals(responseEntity.getBody().getFirstName(), "Emma");
        Assertions.assertEquals(responseEntity.getBody().getLastName(), "L");
    }

    @Test
    void testFindProviderByAvailabilities() {
        // Emma's availabilities are in 2021.1.10, from 9am to 5pm
        final LocalDateTime startDateTime = LocalDateTime.of(2021, 1, 10, 10, 0);
        final LocalDateTime endDateTime = LocalDateTime.of(2021, 1, 10, 14, 0);
        final Availability availability = new Availability(startDateTime, endDateTime);
        ProviderDTO dto = new ProviderDTO();
        dto.setFirstName("Emma");
        dto.setLastName("L");
        dto.setAvailability(availability);
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<ProviderDTO> httpEntity = new HttpEntity<ProviderDTO>(dto, headers);
        ResponseEntity<List> responseEntity = restTemplate.postForEntity(URL + "/provider/availabilities", httpEntity, List.class);
        Assertions.assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
        Assertions.assertTrue(responseEntity.getBody().size() > 0);
    }

    @Test
    void testCannotFindProviderByAvailabilities() {
        // Emma's availabilities are in 2021.1.10, from 9am to 5pm
        final LocalDateTime startDateTime = LocalDateTime.of(2021, 1, 11, 10, 0);
        final LocalDateTime endDateTime = LocalDateTime.of(2021, 1, 11, 14, 0);
        final Availability availability = new Availability(startDateTime, endDateTime);
        ProviderDTO dto = new ProviderDTO();
        dto.setFirstName("Emma");
        dto.setLastName("L");
        dto.setAvailability(availability);
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<ProviderDTO> httpEntity = new HttpEntity<ProviderDTO>(dto, headers);
        ResponseEntity<List> responseEntity = restTemplate.postForEntity(URL + "/provider/availabilities", httpEntity, List.class);
        Assertions.assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
        Assertions.assertFalse(responseEntity.getBody().size() > 0);
    }

    @Test
    void testBookAppointment() {
        final Patient patient = new Patient("Peter", "Q");
        final Provider provider = new Provider("Emma", "L");
        final LocalDateTime startDateTime = LocalDateTime.of(2021, 1, 10, 10, 0);
        final Availability availability = new Availability(startDateTime);
        AppointmentDTO dto = new AppointmentDTO();
        dto.setPatient(patient);
        dto.setProvider(provider);
        dto.setAvailability(availability);

        // before booking
        ProviderDTO providerDto = new ProviderDTO();
        providerDto.setFirstName("Emma");
        providerDto.setLastName("L");
        providerDto.setAvailability(availability);

        HttpHeaders headers = new HttpHeaders();
        HttpEntity<ProviderDTO> httpEntity01 = new HttpEntity<ProviderDTO>(providerDto, headers);

        ResponseEntity<List> responseEntity01 = restTemplate.postForEntity(URL + "/provider/availabilities", httpEntity01, List.class);
        Assertions.assertEquals(responseEntity01.getStatusCode(), HttpStatus.OK);
        Assertions.assertEquals(responseEntity01.getBody().size(), 1);

        // booking
        HttpEntity<AppointmentDTO> httpEntity = new HttpEntity<AppointmentDTO>(dto, headers);
        ResponseEntity<Boolean> responseEntity = restTemplate.postForEntity(URL + "/appointment", httpEntity, Boolean.class);
        Assertions.assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);


        // after booking
        responseEntity01 = restTemplate.postForEntity(URL + "/provider/availabilities", httpEntity01, List.class);
        Assertions.assertEquals(responseEntity01.getStatusCode(), HttpStatus.OK);
        Assertions.assertEquals(responseEntity01.getBody().size(), 0);

    }

}